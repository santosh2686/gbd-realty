(function(a){a.fn.extend({horSlider:function(b){var c={nextArrow:"",prevArrow:"",count:5,speed:600,prevDisableClass:"",nextDisableClass:""};var b=a.extend(c,b);return this.each(function(){function h(){d.find(c.nextArrow).removeClass(c.nextDisableClass);d.find(c.nextArrow).bind("click",g);f--;d.find("ul").animate({left:-d.find("li:eq("+f+")").position().left},c.speed);if(f==0){d.find(c.prevArrow).addClass(c.prevDisableClass).unbind("click")}return false}function g(){d.find(c.prevArrow).removeClass(c.prevDisableClass);d.find(c.prevArrow).bind("click",h);f++;d.find("ul").animate({left:-d.find("li:eq("+f+")").position().left},c.speed);if(f==e-c.count){d.find(c.nextArrow).addClass(c.nextDisableClass).unbind("click");}return false}var c=b;var d=a(this);var e;var f=0;e=d.find("li").size();if(e<=c.count){d.find(c.nextArrow).addClass(c.nextDisableClass);d.find(c.prevArrow).addClass(c.prevDisableClass)}else{d.find(c.nextArrow).bind("click",g)}})}})})(jQuery);

/* Homepage Baneer Slideshow */
$(function(){
var banArray = ['images/01.jpg','images/02.jpg','images/03.jpg','images/04.jpg','images/05.jpg','images/06.jpg'],banHold = $('.banner'),len = banArray.length,ctr = 0,speed = 800,timer,banLoad = $('.loader');	
if(banHold.length != 0)
slideChange(ctr);	

	function slideChange(ind)
	{
		clearInterval(timer);
		banLoad.css({'display':'block'})
		var banImg = new Image()
		$(banImg).load(function(){
			$(this).css({'display':'none'});
			$(this).prependTo(banHold);
			$(this).fadeIn(speed,function(){
				timer = setInterval(slideShow,6000);				
			});
			banLoad.css({'display':'none'})
			
			if(banHold.find('img').length>1)
			{
				banHold.find('img:eq(1)').fadeOut(speed,function(){
					$(this).remove();
				})
			}
		}).attr('src',banArray[ind])		
		
		
		if(ctr==len-1)
		{
			ctr=-1;
		}
	}
	
	
	function slideShow()
	{
		ctr++;
		slideChange(ctr)
	}
	
	
	
	/*Project List Accordian*/	
	var accr = $('#accordian');
	
	accr.find('h4').live('click',function(){
		$(this).next('div').slideToggle(800);
		$(this).toggleClass('active');
		
		$(this).nextAll('h4').removeClass('active');
		$(this).nextAll('h4').next('div').slideUp(800);
		
		$(this).prevAll('h4').removeClass('active');
		$(this).prevAll('h4').next('div').slideUp(800);
		
	})
	
	/*Project Detail popup Box*/
	var prLinks = $('.showInfo'),poupDiv = $('.prInfo'),loadData,loadDiv = $('#loadContent'),prLen=0,prCtr = 0,cInd;
	
	prLinks.find('a[rel="img"]').click(function(){
		cInd = $(this).parents('ul').find('li').index($(this).parent('li'));
		loadData = $(this).attr('href');
		loadImgData(loadData);
		
		if(poupDiv.find('.prNext').is(':visible'))
			poupDiv.find('.prNext').css('display','none');
			
		if(poupDiv.find('.prPrev').is(':visible'))
			poupDiv.find('.prPrev').css('display','none');
		return false;
	})
		
	
	function loadImgData(x){
		$('.prLoader').css({'display':'block'});
		var lImg = new Image()
		
		$(lImg).load(function(){
			$(this).css({'display':'none'});
			$(this).prependTo(loadDiv);
			$(this).fadeIn(800,function(){
				$('.prLoader').css({'display':'none'});
			});
			
			if(loadDiv.find('img').length>1)
			{
				loadDiv.find('img:eq(1)').fadeOut(800,function(){
					$(this).remove();
				})
			}
			
			loadDiv.find('#feature').fadeOut(800,function(){$(this).remove()});
			
		}).attr('src',x)
		
		if(poupDiv.is(':hidden'))
			poupDiv.fadeIn(800);
			
		hightlightLink()
		
	}
	
	prLinks.find('a[rel="mulImg"]').click(function(){
		cInd = $(this).parents('ul').find('li').index($(this).parent('li'));
		loadData = $(this).attr('href').split(',');	
		prLen = loadData.length;
		loadImgData(loadData[prCtr]);
		if(prLen>1)
			poupDiv.find('.prNext').css('display','block');
		return false;
	})
	
	
	poupDiv.find('.prNext').click(function(){
		prCtr++;
		loadImgData(loadData[prCtr]);
		showHideArrow();
	})
	
	
	poupDiv.find('.prPrev').click(function(){
		prCtr--;
		loadImgData(loadData[prCtr]);
		showHideArrow();
	})
	
	
	function showHideArrow()
	{
		if(prCtr>=prLen-1)
			poupDiv.find('.prNext').css({'display':'none'});
		else
			poupDiv.find('.prNext').css({'display':'block'});
			
		if(prCtr<=0)
			poupDiv.find('.prPrev').css({'display':'none'});
		else
			poupDiv.find('.prPrev').css({'display':'block'});			
	}
	
	
	
	/*Click on feature link*/
	
	var lightImg;	
	if($('a[rel="lighbox"]').length != 0 || $('a[rel="lighboxSingle"]').length != 0 )
	{
		$('body').append('<div class="overlay"></div>');
		$('#lightBox').css({'left':($(window).width()-$('#lightBox').width()) / 2, 
		'top':($(window).height()-$('#lightBox').height()) / 2});
	}
	

	prLinks.find('a[rel="lighbox"]').click(function(){
		lightImg = $('.fThumb li:eq(0) a').attr('href');
		loagLight(lightImg);
		$('#lightBox,.overlay').fadeIn(800);
		$('#lightBox').find('.fThumb').show(0);
		return false;
	})
		
	
	
	$('a[rel="lighboxSingle"]').click(function(){
		lightImg = $(this).attr('href');
		loagLight(lightImg);
		$('#lightBox,.overlay').fadeIn(800);
		$('#lightBox').find('.fThumb').hide(0);
		return false;
	})
	
	
	$('.fThumb a').click(function(){
		lightImg = $(this).attr('href');
		loagLight(lightImg);
		return false;
	})
		
	
	function loagLight(y)
	{
		$('#lightBox').find('.fBig').find('p').css({'display':'block'});
		var lgImg = new Image();
		$(lgImg).load(function(){
			$(this).css({'display':'none'});
			$(this).prependTo($('#lightBox').find('.fBig'));
			$(this).fadeIn(800);
			$('#lightBox').find('.fBig').find('p').css({'display':'none'});
			
			if($('#lightBox').find('.fBig').find('img').length>1)
			{
				$('#lightBox').find('.fBig').find('img:eq(1)').fadeOut(800,function(){
					$(this).remove();
				})
			}
			
		}).attr('src',y);
	}
	
	
	
	/*Light Box CLose*/
	$('#closeLight').click(function(){
		$('#lightBox,.overlay').fadeOut(800);
		$('#lightBox').find('.fBig').find('img').remove();
	})
	
	
	/*Close PopUp*/
	$('#closePop').click(function(){
		poupDiv.fadeOut(800);
	})
	
	
	/*Hightlight active link*/
	function hightlightLink()
	{
		prLinks.find('a.active').removeClass('active');
		prLinks.find('a:eq('+cInd+')').addClass('active');
	}
	
	
	
	/*Amenities pop up*/
	prLinks.find('a[rel="data"]').click(function(){
		cInd = $(this).parents('ul').find('li').index($(this).parent('li'));
		loadDiv.empty().html($('.amenities').clone());
		loadDiv.find('.amenities').fadeIn(800);
		poupDiv.fadeIn(800);
		hightlightLink();
		
		if(poupDiv.find('.prNext').is(':visible'))
			poupDiv.find('.prNext').css('display','none');
			
		if(poupDiv.find('.prPrev').is(':visible'))
			poupDiv.find('.prPrev').css('display','none');		
		
		return false;
	})
	
	
});